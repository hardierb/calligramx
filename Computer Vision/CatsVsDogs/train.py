"""
Image classification from scratch, starting from JPEG image files on
the Kaggle Cats vs Dogs binary classification dataset
 """
from typing import Any, Generator, Mapping, Tuple

from absl import app
import haiku as hk
import jax
from jax.experimental import optix
import jax.numpy as jnp
import numpy as np
from .xception import XceptionV1Block, Conv2DBatch, SeparableConv2D

OptState = Any
Batch = Mapping[str, np.ndarray]


def model(batch: Batch) -> jnp.ndarray:
    # data

    # Xception stack
    entry_block = hk.Sequential([Conv2DBatch(32, 3, 2), Conv2DBatch(64, 3)])
    middle_flow = hk.Sequential([XceptionV1Block(128, 3, 2),
                                 XceptionV1Block(256, 3, 2),
                                 XceptionV1Block(512, 3, 2),
                                 XceptionV1Block(728, 3, 2)])
    exit_flow = hk.Sequential([SeparableConv2D(1024, 3)])

    # Core Network
    x = entry_block(x)
    x = middle_flow(x)
    x = exit_flow(x)
    x = jax.nn.relu(x)

    # Global average pooling
    x = hk.avg_pool(x, (x.shape[1], x.shape[2]))

    # Classifier part
    x = hk.Linear(1)(x)
    return x


def binary_cross_entropy(x: jnp.ndarray, logits: jnp.ndarray) -> jnp.ndarray:
    """Calculate binary (logistic) cross-entropy from distribution logits.
    Args:
        x: input variable tensor, must be of same shape as logits
        logits: log odds of a Bernoulli distribution, i.e. log(p/(1-p))
    Returns:
        A scalar representing binary CE for the given Bernoulli distribution.
     """
    if x.shape != logits.shape:
        raise ValueError("inputs x and logits must be of the same shape")

    x = jnp.reshape(x, (x.shape[0], -1))
    logits = jnp.reshape(logits, (logits.shape[0], -1))

    return -jnp.sum(x * logits - jnp.logaddexp(0.0, logits), axis=-1)

def main(_):
    # Make the network and the optmiser
    net = hk.transform(model)
    opt = optix.adam(1e-3)

    # Training Loss: Cross-Entropy
    @jax.jit
    def loss(params: hk.Params, batch: Batch) -> jnd.ndarray:
        """Compute the Cross-entropy + L2 regularization"""
        logits = net.apply(params, batch)
        labels = batch["label"]

        l2_loss = 0.5 * sum(jnp.sum(jnp.square(p)) for p in jax.tree_leaves(params))

        return binary_cross_entropy(labels, logits) + 1e-4 * l2_loss

    # Evaluation metric (classification accuracy).
    @jax.jit
    def accuracy(params: hk.Params, batch: Batch) -> jnp.ndarray:
        predictions = net.apply(params, batch)
        return jnp.mean(jnp.argmax(predictions, axis=-1) == batch["label"])

    @jax.jit
    def update(
            params: hk.Params,
            opt_state: OptState,
            batch: Batch,
    ) -> Tuple[hk.Params, OptState]:
        """Learning rule (stochastic gradient descent)."""
        grads = jax.grad(loss)(params, batch)
        updates, opt_state = opt.update(grads, opt_state)
        new_params = optix.apply_updates(params, updates)
        return new_params, opt_state

    # Make datasets.
    train = load_dataset("train", is_training=True, batch_size=1000)
    train_eval = load_dataset("train", is_training=False, batch_size=10000)
    test_eval = load_dataset("test", is_training=False, batch_size=10000)

    # Initialize network and optimiser; note we draw an input to get shapes.
    params =  net.init(jax.random.PRNGKey(42), next(train))
    opt_state = opt.init(params)

    # Train/eval loop.
    for step in range(10001):
        if step % 1000 == 0:
            # Periodically evaluate classification accuracy on train & test sets.
            train_accuracy = accuracy(avg_params, next(train_eval))
            test_accuracy = accuracy(avg_params, next(test_eval))
            train_accuracy, test_accuracy = jax.device_get(
                (train_accuracy, test_accuracy))
            print(f"[Step {step}] Train / Test accuracy: "
                  f"{train_accuracy:.3f} / {test_accuracy:.3f}.")

        # Do SGD on a batch of training examples.
        params, opt_state = update(params, opt_state, next(train))


if __name__ == "__main__":
    app.run(main)