""" Haiku module for Xception Model"""
from typing import Optional, Sequence, Union
import haiku as hk
import jax
import jax.numpy as jnp


class Conv2DBatch(hk.Module):
    def __init__(self,
                 output_channels:int,
                 kernel_shape: Union[int, Sequence[int]],
                 stride: int = 1,
                 padding="SAME",
                 use_bn: bool = True,
                 name: Optional[str] = None,
                 ):
        super().__init__(name=name)
        self.use_bn = use_bn
        self.conv = hk.Conv2D(output_channels=output_channels,
                             kernel_shape=kernel_shape,
                             stride=stride,
                             padding=padding)

    def __call__(self, inputs: jnp.ndarray, is_training: bool) -> jnp.ndarray:
        out = self.conv(inputs)
        if self.use_bn:
            bn = hk.BatchNorm(create_scale=True, create_offset=True, decay_rate=0.999)
            out = bn(out, is_training)
        out = jax.nn.relu(out)
        return out


class SeparableConv2D(hk.Module):
    """ SeparableConv2D with depthwise separable convolution followed by a pointwise convolution"""

    def __init__(self,
                 output_channels:int,
                 kernel_shape: Union[int, Sequence[int]],
                 use_bn: bool = True,
                 name: Optional[str] = None,
                 ):
        super().__init__(name=name)
        self.output_channels = output_channels
        self.use_bn = use_bn

        self.depthwise = hk.DepthwiseConv2D(
                            channel_multiplier=1,
                            kernel_shape=kernel_shape,
                            padding="SAME",
                            with_bias=False,
                            name="depthwise_conv")

        self.pointwise = hk.Conv2D(
                                output_channels=output_channels,
                                kernel_shape=1,
                                with_bias=False,
                                name="pointwise_conv")

    def __call__(self, inputs: jnp.ndarray, is_training: bool) -> jnp.ndarray:
        """Connects ``SeparableConv2D`` layer.
        Args:
            inputs: A rank-N+2 array with shape ``[N, spatial_dims, C]``.
        Returns:
            A rank-N+2 array with shape ``[N, spatial_dims, output_channels]``.
        """
        out = self.depthwise(inputs)
        out = self.pointwise(out)
        if self.use_bn:
            bn = hk.BatchNorm(create_scale=True, create_offset=True, decay_rate=0.999)
            out = bn(out, is_training)
        return out


class XceptionV1Block(hk.Module):
    """ block with (ReLU -> SeparableConv) * 2 -> MaxPooling with residual"""

    def __init__(self,
                 output_channels: int,
                 kernel_shape: Union[int, Sequence[int]],
                 stride: Union[int, Sequence[int]],
                 name: Optional[str] = None,
                 ):
        super().__init__(name=name)
        self.sep0 = SeparableConv2D(output_channels=output_channels, kernel_shape=kernel_shape)
        self.sep1 = SeparableConv2D(output_channels=output_channels, kernel_shape=kernel_shape)
        self.maxpool = hk.MaxPool(window_shape=kernel_shape,stride=stride)
        self.conv = hk.Conv2D(output_channels=output_channels, kernel_shape=1, stride=stride, padding="SAME")

    def __call__(self, inputs: jnp.ndarray, is_training: bool) -> jnp.ndarray:
        out = jax.nn.relu(inputs)
        out = self.sep0(out, is_training)
        out = jax.nn.relu(out)
        out = self.sep1(out, is_training)
        out = self.maxpool(x)
        return out + self.conv(inputs)
