"""MNIST classifier"""

from typing import Any, Generator, Mapping, Tuple

from absl import app
import haiku as hk
import jax
from jax.experimental import optix
import jax.numpy as jnp
import numpy as np
import tensorflow_datasets as tfds
from .modules import MLP, ConvBlock

OptState = Any
Batch = Mapping[str, np.ndarray]


def lenet(batch: Batch) -> jnp.ndarray:
    """ LeNet-300-100 MLP network"""
    x = batch["image"].astype(jnp.float32) / 255
    mlp = hk.Sequential([
        hk.Flatten(),
        MLP([300, 100, 10])
    ])
    return mlp(x)


def convnet(batch: Batch) -> jnp.ndarray:
    """ Convnet"""
    x = batch["image"].astype(jnp.float32) / 255
    network = hk.Sequential([
        ConvBlock(1, 32, 3, 2),
        ConvBlock(1, 64, 3, 2),
        hk.Flatten(),
        hk.Linear(10)
    ])
    return network(x)


def load_dataset(split: str,
                 *,
                 is_training: bool,
                 batch_size: int,
    ) -> Generator[Batch, None, None]:
    """Loads the dataset as a generator of batches."""
    ds = tfds.load("mnist:3.*.*", split=split).cache().repeat()
    if is_training:
        ds = ds.shuffle(10 * batch_size, seed=0)
    ds = ds.batch(batch_size)
    return tfds.as_numpy(ds)


def main(_):
    # Make the network and the optmiser
    net = hk.transform(lenet)
    opt = optix.adam(1e-3)

    # Training Loss: Cross-Entropy
    @jax.jit
    def loss(params: hk.Params, batch: Batch) -> jnd.ndarray:
        """Compute the Cross-entropy + L2 regularization"""
        logits = net.apply(params, batch)
        labels = jax.nn.one_hot(batch["label"], 10)

        l2_loss = 0.5 * sum(jnp.sum(jnp.square(p)) for p in jax.tree_leaves(params))
        softmax = - jnp.sum(labels * jax.nn.log_softmax(logits))
        softmax /= labels.shape[0]

        return softmax + 1e-4 * l2_loss

    # Evaluation metric (classification accuracy).
    @jax.jit
    def accuracy(params: hk.Params, batch: Batch) -> jnp.ndarray:
        predictions = net.apply(params, batch)
        return jnp.mean(jnp.argmax(predictions, axis=-1) == batch["label"])

    @jax.jit
    def update(
            params: hk.Params,
            opt_state: OptState,
            batch: Batch,
    ) -> Tuple[hk.Params, OptState]:
        """Learning rule (stochastic gradient descent)."""
        grads = jax.grad(loss)(params, batch)
        updates, opt_state = opt.update(grads, opt_state)
        new_params = optix.apply_updates(params, updates)
        return new_params, opt_state

    # Make datasets.
    train = load_dataset("train", is_training=True, batch_size=1000)
    train_eval = load_dataset("train", is_training=False, batch_size=10000)
    test_eval = load_dataset("test", is_training=False, batch_size=10000)

    # Initialize network and optimiser; note we draw an input to get shapes.
    params =  net.init(jax.random.PRNGKey(42), next(train))
    opt_state = opt.init(params)

    # Train/eval loop.
    for step in range(10001):
        if step % 1000 == 0:
            # Periodically evaluate classification accuracy on train & test sets.
            train_accuracy = accuracy(avg_params, next(train_eval))
            test_accuracy = accuracy(avg_params, next(test_eval))
            train_accuracy, test_accuracy = jax.device_get(
                (train_accuracy, test_accuracy))
            print(f"[Step {step}] Train / Test accuracy: "
                  f"{train_accuracy:.3f} / {test_accuracy:.3f}.")

        # Do SGD on a batch of training examples.
        params, opt_state = update(params, opt_state, next(train))


if __name__ == "__main__":
    app.run(main)
