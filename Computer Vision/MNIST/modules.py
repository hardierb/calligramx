"""Some networks implemented in hk.Module applying on MNIST"""

import types
from typing import Callable, Iterable, Optional, Sequence, Union

import haiku as hk
import jax
import jax.numpy as jnp

class MLP(hk.Module):
    """a Multi-Layer Perceptron"""
    def __init__(self,
                 output_sizes: Iterable[int],
                 w_init: Optional[hk.initializers.Initializer] = None,
                 b_init:Optional[hk.initializers.Initializer] = None,
                 with_bias: bool = True,
                 activation: Callable[[jnp.ndarray], jnp.ndarray] = jax.nn.relu,
                 activate_final: bool = False,
                 name: Optional[str] = None):
        super(MLP, self).__init__(name=name)

        self.w_init = w_init
        self.b_init = b_init
        self.with_bias = with_bias
        self.activation = activation
        self.activate_final = activate_final

        layers = []
        for i, output_size in enumerate(output_sizes):
            layers.append(hk.Linear(output_size=output_size,
                                    w_init=w_init,
                                    b_init=b_init,
                                    with_bias=with_bias,
                                    name="linear_%d" % i))
        self.layers = tuple(layers)

    def __call__(self, inputs: jnp.ndarray):
        num_layers = len(self.layers)
        out = inputs
        for i, layer in enumerate(self.layers):
            out = layer(out)
            if i < (num_layers - 1) or self.activate_final:
                out = self.activation(out)
        return out


class ConvBlock(hk.Module):
    """a Convolution-Pooling block:
     the combination of (CONV -> ACTIVATION) * N -> POOL """
    def __init__(self,
                 num_conv: int,
                 output_channels: Union[int, Sequence[int]],
                 kernel_shape: Union[int, Sequence[int]],
                 pool_shape: Union[int, Sequence[int]],
                 stride: Union[int, Sequence[int]] = 1,
                 rate: Union[int, Sequence[int]] = 1,
                 padding: Union[str, Sequence[Tuple[int, int]], PadFnOrFns] = "SAME",
                 with_bias: bool = True,
                 w_init: Optional[hk.initializers.Initializer] = None,
                 b_init: Optional[hk.initializers.Initializer] = None,
                 data_format: str = "NHWC",
                 activation: Callable[[jnp.ndarray], jnp.ndarray] = jax.nn.relu,
                 name: Optional[str] = None):
        super().__init__(name=name)

        self.activation = activation

        conv_layers = []
        for i in range(num_conv):
            conv2d = hk.Conv2D(
                output_channels=output_channels,
                kernel_shape=kernel_shape,
                stride=stride,
                rate=rate,
                padding=padding,
                with_bias=with_bias,
                w_init=w_init,
                b_init=b_init,
                data_format=data_format,
                name="conv2D_%d" % i)
            conv_layers.append(conv2d)

        self. max_pooling2D = hk.MaxPool(
            window_shape=pool_shape,
            strides=stride,
            padding=padding,
            name="max_pooling2D"
        )

        self.conv_layers = tuple(conv_layers)

    def __call__(self, inputs: jnp.ndarray):
        out = inputs
        for conv in self.conv_layers:
            out = conv(inputs)
            out = self.activation(out)
        return self.max_pooling2D(out)
